﻿var Reply = require('./mensagensServidor'); //Classe que gerencia as respostas do servidor

/* Classe do Usuário IRC */
var User = function (socket, server) {

    this.server = server;       //Ponteiro para o servidor
    this.type   = '';           //Tipo do usuário (node | web)

    this.socket  = socket;      //Socket do usuário
    this.replies = new Reply(); //Novo gerenciador de respostas do servidor
    this.id   = -1;             //Identificador único do usuário
    this.host = '';             //Host do usuário (nome do computador)
    this.password = '';         //Senha do usuário
    this.realname = '';         //Nome real
    this.username = '';         //Nome de usuário (usuário logado no computador de origem)
    this.nickname = '';         //Nickname (como o usuário será identificado no IRC)
    this.channels =  [];        //Canais que o usuário está
    this.awayMsg  = '';         //Mensagem setada pelo comando AWAY (:message)
    this.messages = [];         //Armazena até 30 mensagens (para cliente web)
    this.timeout  = null;       //Elimina usuários inativos num certo período de tempo
    this.access     = 0;        //Último acesso do usuário

    //Flags de modos do usuário
    this.mode = [
        ["a", "away",    false], //Setado pelo comando AWAY (:message)
        ["o", "oper",    false], //Operador do servidor. Setado com OPER
        ["i", "visible", true ], //Invisível. Setado com MODE (+|-)i
        ["w", "wallops", false], //Habilita mensagens globais. Setado com MODE (+|-)w
        ["r", "restric", false]  //Conexão restrita (setada pelo servidor) MODE +r
    ];


    this.get = function(attr){
        if(attr in this) return this[attr];
        else return false;
    };

    this.set = function(attr, value){

        if(attr in this) this[attr] = value;
    };

    //Tempo de espera para matar usuário inativo (web)
    this.countdown = function(){

        clearTimeout(this.timeout);

        var user = this;
        var commands = this.server.commands;

        this.timeout = setTimeout(function(){

            if(user.type == 'web')
                commands.QUIT(user, ['']);

        }, 60000); //Um minuto de espera
    };

    //Diz se o usuário completou o registro de conexão ou não
    this.isRegistered = function(){

        return (this.nickname && this.username && this.realname)? true : false;
    };

    //Envia mensagem para o computador de origem
    this.write = function (obj) {

        if(!obj) obj = {sender: '', content: '', id: '', target: ''};

        var msg = JSON.parse(JSON.stringify(obj));

        this.server.debug("User.write()");
        this.server.set('sentCount', '++');

        //Cliente node
        if((this.type == 'node')&&(obj.sender)){

            if(typeof(msg) != 'string'){
                msg.content = (msg.content)? " :"+msg.content : '';
                msg.target  = (msg.target)? " "+msg.target : '';
                var msg = ":"+ msg.sender +" "+ msg.id +""+ msg.target +""+ msg.content;
            }

            this.socket.write(msg+"\r\n");
        }

        //Cliente web
        else if(this.type == 'web'){

            if(typeof(msg) == 'string'){
                msg = {message: msg};
            }

            //Possui request, envia response e apaga socket
            if (this.socket) {

                //Adiciona mensagens da fila
                if (!((this.messages.length)&&(!msg.sender))) this.messages.push(msg);
                msg = JSON.stringify(this.messages);
                this.messages = [];

                //Retorna mensagens para o usuário
                this.socket.end(msg);
                this.socket = null;
            }

            //Não possui request, guarda na fila
            else {
                this.messages.push(msg);

                //Guarda um máximo de 50 mensagens na fila
                if(this.messages.length > 50){
                    this.messages.splice(50,1);
                }
            }
        }

        else if(this.type == 'soap'){

            if(typeof(msg) == 'string'){
                msg = {message: msg};
            }

            if (!((this.messages.length)&&(!msg.sender))) 
                this.messages.push(msg);
          
            this.socket.responses = { element : this.messages };
            this.messages = [];
        }
    };

    //Formula a resposta padrão do protocolo IRC sendo um comando ou uma resposta numérica
    this.response = function(sender, id, content, target) {

        if(typeof(sender) == 'string'){
            if(sender == 'self'){
                sender = this.nickname+"!"+this.username+"@"+this.host;
            }
            else if(sender == 'server'){
                sender = this.server.get("address");
            }
        }
        else{
            sender = sender.nickname+"!"+sender.username+"@"+sender.host;
        }

        if(target == "self") target = this.nickname;

        content = (content)? content : "";
        target  = (target)? target : "";

        id = id.toUpperCase();

        var message = {
            sender: sender,
            id: id,
            target: target,
            content: content
        };

        return {
            message: message,
            user: this,
            broadcast: function(){
                this.user.broadcast(this.message);
            },
            send: function(){
                this.user.write(this.message);
            }
        };
    };

    //Envia resposta do servidor ao computador de origem
    this.reply = function (code, args) {

        if(code.length > 3) code = this.replies.getReply(code, 'name').id;

        else if(code < 10) code = "00"+code;

        else if(code < 100) code = "0"+code;

        else code = code.toString();

        var message = this.replies.message(code, args);

        this.response('server', code, message, 'self').send();

        return message;
    };

    //Retorna modos ativos do usuário
    this.getModes = function () {

        this.server.debug("User.getModes()");

        var flags = "+";

        for(var i=0; i<this.mode.length; i++){
            if(this.mode[i][2]){
                flags += this.mode[i][0];
            }
        }

        return flags;
    };

    //Seta um modo como ativo ou inativo
    this.setMode = function(type, flag) {

        if (typeof(type) === 'string') {

            //Entrada completa +|- flag
            if ((type.length == 2) && (!flag)) {
                type = type[0];
                flag = type[1];
            }

            //Entrada de sinal +|-
            if ((type.length == 1) && (type.match(/[\-\+]/))) {
                type = (type === "+")? true : false;
            }
        }

        for (var i=0; i<this.mode.length; i++) {
            if (this.mode[i][0] === flag) {
                this.mode[i][2] = type;
                return true;
            }
        }

        return false;
    };

    //Retorna verdadeiro ou falso para um modo (disponível pela letra ou nome)
    this.getMode = function (flag){

        flag = flag.toLowerCase();

        var status = false;

        for (var i=0; i<this.mode.length; i++) {
            if ((this.mode[i][0] === flag) || (this.mode[i][1] === flag)) {
                status = this.mode[i][2];
                break;
            }
        }

        return status;
    };

    //Envia uma mensagem a todos os canais que o usuário está conectado
    this.broadcast = function (msg) {

        this.server.debug("User.broadcast('"+msg+"')");

        this.write(msg);

        for (var i=0; i<this.channels.length; i++) {
            this.server.getChannel(this.channels[i]).write(msg, this.id);
        }
    };

    //Adiciona um canal que o usuário está conectado
    this.addChannel = function (channel) {

        this.server.debug("User.addChannel('"+channel+"')");

        this.channels.push(channel);
    };

    //Destrói o socket do usuário (fim da conexão cliente-servidor)
    this.destroy = function () {

    	this.server.debug("User.destroy()");
        
        if(this.type != 'soap')
    	   this.socket.destroy();
    };
};

module.exports = User;