﻿/* Mensagens do Servidor IRC */
function Reply () {

    // Importa o arquivo de erros
    this.replies = require('../../files/replies');


    //Monta e retorna a mensagem para o usuário
    this.message = function(code, args) { //code: código numérico ou nome da mensagem

        //Recupera a mensagem
        var rpl = (code.length > 3)? this.getReply(code, "name") : this.getReply(code, "id");
        var message = rpl.message;
        
        //Se tiver parâmetros, monta a mensagem
        if(args){
            
            var regex = /<([\w ]+)>/g;
            var matches;
            var values = [];

            while (matches = regex.exec(rpl.message)) {
              values.push(matches[1]);
            }

            for(i = 0; i < values.length; i++){
                message = message.replace("<"+values[i]+">", args[values[i]]);
            }
        }

        return (message);
    };

    //Recupera uma mensagem com seu nome ou código numérico
    this.getReply = function(searchTerm, property) {

        var array = this.replies;

        if(!property) property = "id";

        searchTerm = searchTerm.toString();

        if((property == "id")&&(searchTerm.length < 3)){
            if(searchTerm.length == 1){
                searchTerm = "00"+searchTerm;
            }
            else if(searchTerm.length == 2){
                searchTerm = "0"+searchTerm;
            }
        }

        for(var i = 0, len = array.length; i < len; i++) {
            if (array[i][property] === searchTerm) return array[i];
        }

        return -1;
    };
}

module.exports = Reply;