/* Classe do Canal do servidor IRC */
function Channel(server, name, op) {
    
    var server   = server; //Ponteiro do servidor

    this.name    = name;   //Nome do canal
    this.users   = [];     //Lista de usuários do canal
    this.opers   = [];     //Lista de operadores do canal
    this.creator = op.id;  //Criador do canal
    this.topic   = "";     //Tópico do canal
    this.usersban = [];     //lista de usuário banidos


    this.get = function(attr){
        if(attr in this) return this[attr];
        else return false;
    };

    this.set = function(attr, value){

        if(attr in this) this[attr] = value;
    };

    //Adiciona um usuário à lista de usuários do canal
    this.addUser = function(user){
    	
    	server.debug("Channel.addUser()");

		this.users.push({
			id: user.id, 
			nickname: user.nickname
		});

    	user.addChannel(this.name);
    };

    //Encontra um usuário pelo seu ID ou Nickname
    this.getUser = function(id, attr){ //Retorna o Objeto do Usuário ou o atributo especificado (opcional)
    	
    	server.debug("Channel.getUser()");

        var type = (typeof(id) == 'string')? 'nickname' : 'id';

        var response = false;

        for (var i=0; i<this.users.length; i++) {
            
            if (this.users[i][type] == id) {
                
                if (attr == 'index') {
                    response = i;
                }
                else if (attr) {
                    response = this.users[i][attr];
                }
                else response = this.users[i];

                break;
            }
        }

    	return response;
    };

    //Remove um usuário da lista de usuários do canal
    this.removeUser = function(user){
    	
    	server.debug("Channel.removeUser()");

    	this.users.splice(this.getUser(user.id, 'index'), 1);
    	user.channels.splice(user.channels.indexOf(this.name), 1);

        if ((this.name[0] != "!") && (this.users.length == 0)) {
            server.removeChannel(this.name);
        }
    };

    //Bane um usuário do canal
    this.banUser = function(user){
        
        server.debug("Channel.banUser()");

        this.usersban.push({
            id: user.id,
            nickname: user.nickname
        });

        this.removeUser(user);
    };

    //Encontra um usuário banido  pelo seu ID ou Nickname
    this.getBan = function(id, attr){ //Retorna o Objeto do Usuário ou o atributo especificado (opcional)
        
        server.debug("Channel.getBan()");
        
        var type = (typeof(id) == 'string')? 'nickname' : 'id';
        var response = false;
        
        for (var i=0; i<this.usersban.length; i++) {
            if (this.usersban[i][type] == id) {
                if (attr == 'index') {
                    response = i;
                }
                else if (attr) {
                    response = this.usersban[i][attr];
                }
                else response = this.usersban[i];
                break;
            }
        }
        return response;
    };

    //Lista usuários banidos do canal
    this.listban = function(){

        server.debug("Channel.listBan()");

        var list = '';
        var user = null;
        var count = 0;

        for(var i=0; i<this.usersban.length; i++){
            user = server.getBan(this.usersban[i].id);
            count++;
            list += user.nickname;
        }

        return {
            text: list.replace(/^\s/, ''),
            count: count
        };
    };

    this.list = function(){

        var list = '';
        var user = null;
        var count = 0;
        
        for(var i=0; i<this.users.length; i++){

            user = server.getUser(this.users[i].id);

            if (user) {
                if (user.getMode('i')) { //Só lista os visíveis
                    
                    count++;

                    if(this.creator == user.id){
                        list += " @"+ user.nickname;
                    }
                    else if(this.isOper(user.id)){
                        list += " @"+ user.nickname;
                    }
                    else {
                        list += " " + user.nickname;
                    }
                }
            }
        }

        return {
            text: list.replace(/^\s/, ''), 
            count: count
        };
    };

    //Broadcast para todos os usuários do canal
    this.write = function(message, sender){
    	
    	server.debug("Channel.write('"+message+"')");

        for (var i=0; i<this.users.length; i++) {
            
            user = this.users[i];
            
            if((!sender)||(user.id != sender)){ //Não envia para o autor da mensagem
                
                if(u = server.getUser(user.id))
                    u.write(message);
            }
        }
        //Printa um log no console do servidor com a mensagem do broadcast
        server.debug("["+this.name+"] Broadcast: "+ JSON.stringify(message) + "\r\n");
    };

    this.addOper = function(op){
    	
    	server.debug("Channel.addOper()");

    	this.opers.push(op.id);
    };

    //Recebe o ID do usuário e retorna booleano
    this.isOper = function(id){
    	
    	server.debug("Channel.isOper()");

    	return (this.opers.indexOf(id)>=0)? true : false;
    };

    //Adiciona o criador do canal na lista de membros e operadores
    this.addUser(op);
    this.addOper(op);
}

module.exports = Channel;