var express = require('express'),
    soap = require('soap'),
    path = require('path');

var app = express();

var ircServer;

var parser = {
    ws: {
        soapParser: {
            executeCommand: function (args, cb, headers, req) {
                var pass = args.pass;
                var command = args.command;
                var arguments = args.arguments;
                var parameters = [];
                var callback = {};
                var address = req.headers['x-forwarded-for'] || req.connection.remoteAddress;

                if (arguments != null) {
                    console.log("Not Null: " + arguments.element[0]);
                    for (var i = 0; i < arguments.element.length; i++) {
                        parameters[i] = arguments.element[i];
                    }
                }

                var user = ircServer.getUser(address, pass);

                if (user) {

                    user.countdown();
                    user.set('socket', callback);

                    if (!command)
                        user.write();

                    else if (!user.isRegistered()) {
                        if ((command == 'NICK') || (command == 'USER')) {
                            ircServer.send(user, { command: command, args: parameters });
                            return callback;
                        }
                        else {
                            user.reply(451); //ERR_NOTREGISTERED
                            return callback;
                        }
                    }
                    //Executa comando
                    else {
                        ircServer.send(user, { command: command, args: parameters });
                        return callback;
                    }
                }
                //Usuário não existe
                else if (command == "PASS") {
                    user = ircServer.setWebServiceUser(callback, address, pass, "soap");
                    user.response("self", "PASS").send();
                    return callback;
                }
                //Usuário não existe e conexão não foi registrada
                else {
                    return { response: element = ['Invalid request'] };
                }
            }
        }
    }
};

function WebServiceSoap(server, port) {
    this.server = server;
    this.port = port;
    ircServer = server;

    var xml = require('fs').readFileSync(path.join(__dirname, '../../files') + '/ircwsdl.xml', 'utf8');

    app.listen(this.port, function () {
        var host = this.address().address;
        var port = this.address().port;

        soap.listen(app, '/soap', parser, xml);

        console.log("Web Service SOAP escutando em http://%s:%s", host, port);
        console.log("Requisições SOAP no context /soap");
    });
};

WebServiceSoap.initialize = function (server, port) {
    var webservice = new WebServiceSoap(server, port);
};

exports.WebServiceSoap = WebServiceSoap;