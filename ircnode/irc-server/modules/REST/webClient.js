var events = require('events');


//Interface pela qual o servidor IRC irá interagir
function WebClient (server) {
    
	this.server = server;      // Ponteiro do Servidor Web
	this.id = null;            // ID de gerenciamento do Servidor Web
    this.name = null;          // ID de gerenciamento do Servidor IRC
    this.remoteAddress = null; // IP do usuário
    this.pass = null;          // Senha da conexão (opcional)
    this.messages = [];        // Lista de mensagens do servidor
    this.timeout = null;       // Contagem regressiva de usuário inativo
    this.time = 60000;         // Tempo que o usuário pode ficar ocioso
}

//Seta emissor de eventos
WebClient.prototype = events.EventEmitter.prototype;

//Método de escrita utilizado pelo servidor IRC
WebClient.prototype.write = function (message) {
	//Traduz para a linguagem do servidor Web e salva na lista
	this.messages.push(this.server.parseWeb(message));
};

//Método de encerramento utilizado pelo servidor IRC
WebClient.prototype.destroy = function () {
	//Envia para tratamento do servidor Web
	this.server.removeUser(this.id);
	this.removeAllListeners('data');
};

//Trigger 'data' - Envia mensagem ao servidor IRC
WebClient.prototype.data = function (text) {
	this.countdown();
	this.emit('data', text);
};

//Retorna lista de mensagens armazenadas
WebClient.prototype.getMessages = function () {
	this.countdown();
	var response = JSON.stringify(this.messages);
    this.messages = [];
    return response;
};

//Contagem regressiva para excluir usuário inativo
WebClient.prototype.countdown = function () {

	var client = this;

	clearTimeout(client.timeout);
	
	client.timeout = setTimeout(function(){
		client.data('QUIT');
	}, client.time);
};

module.exports = WebClient;