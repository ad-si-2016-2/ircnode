﻿var express = require('express'), 
    httpserver = require('http'), 
    path = require('path'), 
    bodyParser = require('body-parser'), 
    WebClient = require('./webClient');


var app = express();

function WebServiceRest(server, port) {

    this.server = server;
    this.port   = port;
    this.users = [];

    var ws = httpserver.createServer(app);

    ws.listen(this.port, function () {
        var host = ws.address().address;
        var port = ws.address().port;

        console.log("Web Service escutando em http://%s:%s", host, port);
        console.log("Requisições REST no context /rest");
    });
}

WebServiceRest.initialize = function (server, port) {
    var webservice = new WebServiceRest(server, port);
    webservice.init();
};

WebServiceRest.prototype = {

    init: function initializeEntryPoints() {

        var webservice = this;

        app.use(bodyParser.json());
        app.use(bodyParser.urlencoded({
          extended: true
        }));

        app.all('/*', function (req, res, next) {
            res.header("Access-Control-Allow-Origin", "*");
            res.header("Access-Control-Allow-Headers", "X-Requested-With");
            next();
        });

        //Imprime o cliente web
        app.get(/\/client\//, function (req, res) {

            var url = req.originalUrl.match(/\/client\/(.*)/);

            if ((!url) || (!url[1])) {
                res.sendFile(path.join(__dirname, '../../../irc-web/', 'index.html'));
            }
            else {
                var dir = url[1].match(/^(.*)\/(.*)$/)[1];
                var file = url[1].match(/^(.*)\/(.*)$/)[2];
                res.sendFile(path.join(__dirname, '../../../irc-web/' + dir, file));
            }
        });
        
        //Recebe comando
        app.post('/rest/:command', function (req, res) {

            //Extrai dados e converte para o protocolo IRC
            var command = req.params.command;
            var args    = req.body.args;
            var pass    = (req.body.pass)? req.body.pass : false;
            var ip      = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
            var irc     = webservice.parseIRC(command, args);

            //Identifica usuário
            var user = webservice.getUser(ip, pass);
            
            //Se não existir, cria
            if (!user) {
                user = webservice.addUser(ip, pass);
                webservice.server.connection(user);
            }

            //Envia comando
            user.data(irc);

            //Retorna status de sucesso
            res.status(200).end();
        });

        //Retorna mensagens armazenadas
        app.get('/rest/:pass', function (req, res) {

            //Extrai dados
            var pass    = (req.params.pass)? req.params.pass : false;
            var ip      = req.headers['x-forwarded-for'] || req.connection.remoteAddress;

            //Identifica usuário
            var user = webservice.getUser(ip, pass);

            //Envia mensagens
            if(user)
            res.status(200).end(user.getMessages());
        });
    }, 

    //Adiciona usuário Web
    addUser: function (ip, pass) {

        var user = new WebClient(this);
        user.id = this.users.length;
        user.remoteAddress = ip;
        if (pass) user.pass = pass;
        

        this.users.push(user);

        return user;
    }, 

    //Recupera usuário Web
    getUser: function (ip, pass) {

        var user = false;

        this.users.forEach(function (u) {
            if ( (u.remoteAddress == ip) && ( ((pass)&&(u.pass == pass)) || (!pass)) )
                user = u;
        });

        return user;
    }, 

    //Remove usuário Web
    removeUser: function (id) {

        if (this.users[id])
            delete this.users[id];
    }, 

    //Converte entrada JSON em entrada IRC
    parseIRC: function (command, args) {

        var irc = command.toUpperCase();

        if (args) {
            for (var i=0; i<args.length; i++) {

                irc += ((args[i].match(/\s/))? ' :' : ' ') + args[i];
            }
        }

        return irc;
    }, 

    //Converte saída IRC em saída JSON
    parseWeb: function (text) {

        // Extrai ID e Sender
        var irc = text.match(/^:([^\s]+) ([\w]+)(.*)?/);
        
        var sender = irc[1];
        
        var response = {
            sender: {
                type: ''
            }, 
            id: irc[2].toUpperCase(), 
            type: '', 
            target: '', 
            content: ''
        };

        // Define tipo de resposta
        response.type = (response.id.match(/^([\d]+)$/))? 'reply' : 'command';

        // Identifica tipo e extrai dados do Sender
        if (match = sender.match(/^([^\s]+?)!([^\s]+?)@([^\s]+?)$/)) {
            response.sender.type = "user";
            response.sender.nick = match[1];
            response.sender.user = match[2];
            response.sender.host = match[3];
        }
        else {
            response.sender.type = "server";
            response.sender.host = sender;
        }

        // Extrai Target e Content
        if (irc.length == 4) {
            
            var match;
            
            //Cenário 1: content
            if (match = irc[3].match(/^ :(.*)/)) {
                response.content = match[1];
            }

            //Cenário 2: target
            else if (match = irc[3].match(/^ ([^\s]+)$/)) {
                response.target = match[1];
            }

            //Cenário 3: target + content
            else if (match = irc[3].match(/^ ([^\s]+) :?(.*)/)) {
                response.target  = match[1];
                response.content = match[2];
            }
        }

        return response;
    }
};

exports.WebServiceRest = WebServiceRest;