var Server = require('../modules/Servidor/server.js').Server;
var WebServiceRest = require('../modules/REST/webServiceRest.js').WebServiceRest;
var WebServiceSoap = require('../modules/SOAP/webServiceSoap.js').WebServiceSoap;

var irc = Server.initialize();
WebServiceRest.initialize(irc,8081);
WebServiceSoap.initialize(irc,8082);