Testes usando Frisby.js
=================

Frisby � um framework de testes API REST constru�do a partir do node.js e Jasmine que pega os endpoints da aplica��o REST e realiza testes f�ceis, r�pidos e divertidos.
A documenta��o detalhada da API � encontrada neste [link](http://frisbyjs.com/docs/api/).

Getting Started
---------------

### Instala��o do m�dulo

```Console
sudo npm install jasmine-node

sudo nam install frisby
```

Para o uso devemos instanciar seu m�dulo:

```C#
var frisby = require ('frisby');
```

Os testes podem ser constru�dos de acordo com sua API, mencionada acima.


Rodando os testes automatizados
---------------

```Console
jasmine-node <arquivo>-spec.js
```

O sufixo -spec � utilizado como conven��o. 
