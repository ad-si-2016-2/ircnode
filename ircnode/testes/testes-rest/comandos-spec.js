var frisby = require ('frisby');

var client = {
	pass: 223, 
	user: "web_client", 
	host: "web.irc.inf.ufg.br", 
	realname: "guest", 
	nickname: "WIRED", 
	channel: "#ad-si-2016-2"
};

var channel = {
    name: "channel-test"
};

function init() {
	autenticacao();
	testeJOIN();
	testeNICK();
	testePRIVMSG(channel.name);
	testePRIVMSG('joao');
	testeNOTICE(channel.name);
	testeNOTICE('joao');
	testeMOTD();
	testeLUSERS();
	testeVersion();
	testeTodosStatus();
}

function testeTodosStatus() {
	//teste Status Conexoes
	testeStatus("L");
	//teste Status Comands Count
	testeStatus("M");
	//teste Status Server Operators
	testeStatus("O");
	//teste Status Life Time
	testeStatus("U");
}

function autenticacao() {
	frisby.create('Autenticacao da Senha')
		.get('http://localhost:8081/rest/' + client.pass + '/PASS')
		.expectStatus(200)
		.after(function() {
			frisby.create('Autenticacao do usuario')
			.get('http://localhost:8081/rest/' + client.pass + '/USER/' + client.nickname + '/0/*/' + client.realname)
			.expectStatus(200)	
			.toss();
		})
		.toss();
}

function testeJOIN() {
	frisby.create('Teste JOIN')
		.get('http://localhost:8081/rest/' + client.pass + '/JOIN/' + channel.name)
		.expectStatus(200)
		.toss();
}

function testeNICK() {
	frisby.create('Teste NICK')
		.get('http://localhost:8081/rest/' + client.pass + '/NICK/novo-nick')
		.expectStatus(200)
		.toss();
}

function testePRIVMSG(to) {
	frisby.create('Teste PRIVMSG para ' + to)
		.get('http://localhost:8081/rest/' + client.pass + '/PRIVMSG/' + to)
		.expectStatus(200)
		.toss();
}

function testeNOTICE(to) {
	frisby.create('Teste NOTICE para ' + to)
		.get('http://localhost:8081/rest/' + client.pass + '/NOTICE/' + to)
		.expectStatus(200)
		.toss();
}

function testeMOTD() {
	frisby.create('Teste MOTD')
		.get('http://localhost:8081/rest/' + client.pass + '/MOTD')
		.expectStatus(200)
		.toss();
}

function testeLUSERS() {
	frisby.create('Teste LUSERS')
		.get('http://localhost:8081/rest/' + client.pass + '/LUSERS')
		.expectStatus(200)
		.toss();
}

function testeVersion() {
	frisby.create('Teste Version')
		.get('http://localhost:8081/rest/' + client.pass + '/VERSION')
		.expectStatus(200)
		.toss();
}

function testeStatus(args) {
	frisby.create('Teste STATUS ' + args)
		.get('http://localhost:8081/rest/' + client.pass + '/STATUS/' + args)
		.expectStatus(200)
		.toss();
}

init();
