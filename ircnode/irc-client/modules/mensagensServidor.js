function Reply (language) {

    //Importa arquivo traduzido
    this.replies  = null;
    this.commands = null;
    this.language = language;
    
    this.setLang = function (lang){
        try{
            this.replies  = require('../files/replies/'+lang);
            this.commands = require('../files/commands/'+lang);
            return true;
        } 
        catch(e){
            return false;
        }
    };

    this.setLang(language);

    /* Monta e envia a mensagem para o usuário */
    this.message = function(code, args){
        if(typeof(args) === "string"){
            return this.reply(code, args);
        }
        else{
            return this.command(code, args);
        }
    };

    this.command = function(code, args){

        if(typeof(code) === "string"){
            var message = this.getCommand(code).message;
        }
        else{
            var message = this.commands[code].message;
        }

        var keys = message.match(/<([\w\-_]+)>/g);
        
        keys.forEach(function(param){
            message = message.replace(param, args[param.replace(/[<>]/g, '')]);
        });

        return message;
    };

    this.getCommand = function(code){
        for(var i=0; i<this.commands.length; i++){
            if(this.commands[i].command === code){
                return this.commands[i];
            }
        }
        return false;
    };

    this.reply = function(code, msg){

        //Se for linguagem padrão, não precisa processar
        if(!this.language == "en-US")
            return msg;

        //Recupera mensagem original
        var rpl = this.getReply(code);

        //Se não houver tradução, retorna mensagem original
        if(!rpl.translate)
            return msg;

        //Prepara para montar mensagem traduzida
        var keys    = rpl.message.match(/<([\w ]+)>/g);
        var pattern = rpl.message.replace(/<([\w ]+)>/g, "(.*)");
        var regex   = new RegExp(pattern);
        var values  = regex.exec(msg);
        var translate = rpl.translate;
        
        if((keys)&&(values)){
            //Monta mensagem traduzida
            for(var i=0; i<keys.length; i++){
                if(translate.match(keys[i]))
                    translate = translate.replace(keys[i], values[i+1]);
            }
        }

        return translate;
    };

    this.getReply = function (searchTerm, property) {
        
        if(!property) property = "id";
        
        for(var i = 0, len = this.replies.length; i < len; i++) {
            if (this.replies[i][property] == searchTerm) return this.replies[i];
        }
        
        return false;
    };

}

module.exports = Reply;