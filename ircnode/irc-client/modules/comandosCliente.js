var ServerReplies = require('./mensagensServidor');
var	fs = require('fs');

/*    Esta classe serve para analisar a mensagem recebida   **
**      e executar as devidas ações para cada uma delas     **
**  posteriormente montando e printando na tela do usuário  **
**               em qualquer formato desejado.              */

function Commands(client) {
    
    this.client = client;	
    this.response = new ServerReplies(client.language);
    this.channels = [];
	    
    //
    this.reply = function(args){

    	switch(args.code){

    		case "001":
    			//Seta o nickname enviado pela mensagem de registro bem sucedido
    			this.client.set('nickname', args.receiver);
    		break;

    		case "353":
    			
    			var matches = args.content.match(/([#!&\+])([\w\-_\*]+)/);
    			
    			if((matches)&&(matches[0] === client.get('channel'))){

    				var channel = matches[0];

	    			//Pega lista de usuários do canal e salva seus prefixos
	    			matches = args.content.match(/^([=+@]) (.*) :(.*)/);
	    			var list = matches[3].split(" ");
	    			var prefix = "";
					this.channels[channel] = [];

	    			for(var i=0; i<list.length; i++){
	    				prefix = "";
	    				if(list[i][0].match(/[@+!]/)){
	    					prefix = list[i][0];
	    					list[i] = list[i].replace(/[@+!]/, '');
	    				}
	    				this.channels[channel][list[i]] = prefix;
	    			}
	    		}
    		break;

    	}


		//Envia para o tratamento de respostas e printa na tela do usuário
    	client.print(this.response.message(args.code, args.content).replace(/^:/, ''));
    };

    //
    this.command = function(args){

    	switch(args.command){
    		
    		case "NICK":
	    		if(args.nick == client.get("nickname")){
	    			client.set("nickname", args.content);
	    		}

	    		client.print( this.response.message("NICK", {'old': args.nick, 'new': args.content}) );
    		break;


    		case "JOIN":
    			if(args.nick == client.get("nickname")){
    				client.set("channel", args.receiver);
    			}

	    		if(args.receiver === client.get('channel')) //Printa se for o canal atual
    				client.print( this.response.message("JOIN", {'nick': args.nick, 'channel': args.receiver}) );
    		break;


    		case "PART":
    			if(args.receiver === client.get('channel')) //Printa se for o canal atual
    				client.print( this.response.message("PART", {'nick': args.nick, 'channel': args.receiver}) );
    		break;


    		case "PRIVMSG":
    			if (args.receiver[0].match(/[#!&+]/)){
    				if(args.receiver === client.get('channel')){ //Printa se for o canal atual (evitar flood na tela do usuário)
    					
    					//Seta prefixo do usuário
    					var prefix = this.channels[args.receiver][args.nick]
    					var nick = (prefix)? prefix + args.nick : args.nick;
    					
    					client.print( this.response.message("MSG", {'nick': nick, 'message': args.content}) );
    				}
    			}
    			else if (args.receiver === client.get('nickname')){
    				client.print( this.response.message("PRIVMSG", {'nick': args.nick, 'message': args.content}) );
    			}
    		break;


    		default:
    			client.print(args.command+" "+args.content);
    	}
    };

    //
    this.getConfig = function(attr, value){
    	
    	var config = client.get("config");

    	//Comando get específico
		if(attr){
			if(config[attr]){
				client.print("-- "+ attr.toUpperCase() +" "+ config[attr]);
			}
			else if(config[attr] == ""){
				client.print("-- "+ attr.toUpperCase() +" not configured");
			}
			else{
				client.print("-- No such config");
			}
		}
		//Comando get global
		else{
			Object.keys(config).forEach(function (key) {
				client.print("-- " + key.toUpperCase() + " "+ config[key]);
			});
		}
    };

    //
    this.setConfig = function(attr, value){

    	var config = client.get("config");

    	//Nickname só pode ter até 9 caracteres
		if((attr == "nickname")&&(value.length > 9)){
			client.print("-- NICKNAME can only have 9 characters");
		}
		//Executa a modificação
		else if(config[attr] != undefined){

			//Se a linguagem for inválida
			if((attr == "language")&&(!this.response.setLang(value))){
				client.print("-- Invalid language");
				return;
			}
			//Seta e persiste a configuração
			else{
				if(value == 'true')  value = true;
				if(value == 'false') value = false;
				config[attr] = value;
			}
			fs.writeFile('../.ircnode.conf.json', JSON.stringify(config));

			//Seta as configurações no cliente
			if((attr != "nickname")&&(attr != "channel")){
				client.set(attr, config[attr]);
				client.set('_address', config.server);
				client.set('_port', config.port);
			}

			client.print("-- "+ attr.toUpperCase() +" changed");

			//Muda o nickname no servidor também
			if((attr == "nickname")&&(value != client.get("nickname"))){
				client.send("/NICK "+value);
			}
		}
		//Configuração inexistente
		else{
			client.print("-- No such config");
		}
    };
}

module.exports = Commands;