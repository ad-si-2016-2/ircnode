const readline     = require('readline');
const rl           = readline.createInterface(process.stdin, process.stdout);
var server         = require('./conexao'), 
	net            = require('net'), 
	path           = require('path'), 
	ClientCommands = require('./comandosCliente'), 
	config         = require('../.ircnode.conf');


function Client () {

	this.config   = config;
	this.username = (process.platform == 'win32')? process.env['USERPROFILE'].split(path.sep)[2] : process.env['HOME'].split(path.sep)[1];
	this.nickname = config.nickname;
	this.realname = config.realname;
	this.channel  = config.channel;
	this.version  = config.version;
	this.language = config.language;
	this.debug    = config.debug;

	this._address = config.server;
	this._port    = config.port;
	this._socket  = null;
	this.commands = new ClientCommands(this);

	this.get = function(attr){
		return this[attr];
	};
	this.set = function(attr, value){
		this[attr] = value;
	};
}

Client.prototype = {

	connect: function () {

		this.print("Connecting to IRC server: " + this._address + " on port " + this._port);
        this.print("Type /HELP for a list of commands allowed");
        
		server.connect(this, {
			nickname: this.nickname, 
			realname: this.realname, 
			username: this.username, 
			channel : this.channel,
		});

		this.read();
	}, 

	data: function (msg){
		
		var message = [];
		var content;
		var args;

		if(this.debug)
			console.log("** "+ msg);

		// Primeira checagem (Server)
		// [1]:server [2]:message
		if(args = msg.match(/^:([\w.]+) (.*)/)){
			message["sender"] = "server";
			message["server"] = args[1];
			content = args[2];
		}
		// Primeira checagem (User)
		// [1]:Nick [2]:User [3]:host [4]:message
		else if(args = msg.match(/^:(.*)!(.*)@([\w.\/\\:]+) (.*)/)){
			message["sender"] = "user";
			message["nick"] = args[1];
			message["user"] = args[2];
			message["host"] = args[3];
			content = args[4];
		}
		//Fora do padrão do protocolo
		else{
			this.print(msg);
			return;
		}

		// Segunda checagem (Reply)
		// [1]:code [2]:nick [3]:message
		if(args = content.match(/^([\d]+) ([\w\-_*#!&+]+) (.*)/)){
			message["type"] = "reply";
			message["code"] = args[1];
			message["receiver"] = args[2];
			message["content"] = args[3].replace(/^:/, '');
			
			this.commands.reply(message);
		}
		// Segunda checagem (Command)
		// [1]:command [2]:nick [3]:message
		else if(args = content.match(/^([\w]+) (.*)/)){

			message["type"] = "command";
			message["command"] = args[1].toUpperCase();
			message["content"] = args[2];

			if(args = message["content"].match(/^([\w\-_*#!&+]+) (.*)/)){
				message["receiver"] = args[1];
				message["content"]  = args[2].replace(/^:/, '');
			}
			else if(args = message["content"].match(/^:(.*)/)){
				message["content"]  = args[1].replace(/^:/, '');
			}
			else if(args = message["content"].match(/^([\w\-_*#!&+]+)/)){
				message["receiver"] = args[1];
			}

			this.commands.command(message);
		}
		//Fora do padrão do protocolo
		else{
			this.print(msg);
			return;
		}
	}, 

	send: function (message) {

		if (message[0] == "/") {
			var msg = message.slice(1, message.length);
			this._socket.write(msg + "\r\n");
		}
		else {
			var msg = "MSG "+ this.channel +" :"+ message;
			this._socket.write(msg + "\r\n");
		}
	}, 

	print: function (msg) {

		if(!msg.match(/^(\*[\w-_!@+]+\*|\<[\w-_!@+]+\>|\-\-)/)){
			msg = "== " + msg;
		}

		var d = new Date();
		var h = d.getHours();
		var m = d.getMinutes();
		var s = d.getSeconds();

		msg = msg.replace(/\n/g, '');

		var timestamp = ((h<10)?'0'+h:h) + ":" + ((m<10)?'0'+m:m) + ":" + ((s<10)?'0'+s:s);

		process.stdout.clearLine();
		process.stdout.cursorTo(0);
		process.stdout.write("["+ timestamp + "] " + msg + "\r\n" + this.pointer());
		process.stdout.cursorTo(11);
	}, 

	pointer: function() {
		//Monta um ponteiro no estilo "nick     > "
		var pointer = [" ", " ", " ", " ", " ", " ", " ", " ", " ", ">", " "];
		
		for(var i=0; i<this.nickname.length; i++){
			pointer[i] = this.nickname[i];
		}
		return pointer.toString().replace(/,/g, "");
	}, 

	//Trata as entradas de texto do usuário e confere se é um comando interno
	input: function(command){

		var args = command.match(/\/(get|set)-config($| ([\w]+)($| (.*)))/);

		//Linha em branco, ignora
		if(!command){
			return false;
		}
		//Comando /get|set-config
		else if(args){

			var action = args[1];
			var attr   = (args[3])? args[3].toLowerCase() : false;
			var value  = (args[5])? args[5].replace(/['"]/g, '') : false;

			//Comando get-config
			if(action == "get")
				this.commands.getConfig(attr, value);

			//Comando set-config
			else if((action == "set")&&(attr&&value))
				this.commands.setConfig(attr, value);

			return false;
		}
		//Conecta ao servidor IRC
		else if(command.match(/^\/(CONNECT|connect)$/)){
			if(this._socket) this._socket.destroy();
			this.connect();
			return false;
		}
		//Limpa a tela do console
		else if(command.match(/^\/(CLEAR|clear)$/)){
			process.stdout.write('\u001B[2J\u001B[0;0f');
			return false;
		}
		//Não é um comando interno do Cliente
		else{
			return true;
		}
	}, 

	//Lê as entradas do usuário
	read: function () {
		
		var client = this;
		
		rl.question(this.pointer(), function (command) {
			
			//Se não for um comando interno, envia para o servidor
			if(client.input(command)) client.send(command);

			//Retorna a ler o console
			client.read();
		});
	}
};

exports.Client = Client;