/* Tratamento de comandos do servidor IRC */

function Commands() {
	
	this.execute = function (data) {
		//Confere se existe tratamento para esse comando, se existir executa
		if (typeof this[data.id] === 'function') this[data.id](data);
	};
}

Commands.prototype.JOIN = function (data) {

	if (data.sender.nick == chat.client.nick) {
		var talk = new Talk(data.target, 'channel');
		talks.add(talk, true);
	}
	else {
		talks.addMember(data.target, data.sender.nick);
	}
	talks.notice('<b>'+data.sender.nick+'</b> entrou no canal', data.target, data.sender.nick);

};

Commands.prototype.PART = function (data) {

	if(data.sender.nick == chat.client.nick){
		talks.remove(data.target);
	}
	else{
		talks.removeMember(data.target, data.sender.nick);
		talks.notice("<b>"+data.sender.nick+"</b> saiu do canal", data.target, data.sender.nick);
	}

};

Commands.prototype.QUIT = function (data) {

	if(data.sender.nick == chat.client.nick){
		talks.remove(data.target);
	}
	else{
		talks.removeMember(data.target, data.sender.nick);
		talks.notice("<b>"+data.sender.nick+"</b> se desconectou", data.target, data.sender.nick);
	}

};

Commands.prototype.NICK = function (data) {

	if (data.sender.nick === chat.client.nick) {

		chat.client.nick = data.content;
		$('#user-nickname').val(data.content);
		talks.notice('<b>'+data.sender.nick+'</b> mudou seu nome para <b>'+data.content+'</b>', null, data.sender.nick);
	}

	else {

		var notice = '<b>'+data.sender.nick+'</b> mudou seu nome para <b>'+data.content+'</b>';

		if (talks.exists(data.sender.nick)) {
			
			var messages = talks.get(data.sender.nick, 'messages');
			var select = talks.isSelected(data.sender.nick);
			
			var talk = new Talk(data.content, 'user');
			talk.set('messages', messages);
			
			talks.add(talk, select);
			talks.remove(data.sender.nick);

			talks.notice(notice, data.content, data.sender.nick);
		}

		var channels = talks.member(data.sender.nick);

		for (var i=0; i<channels.length; i++) {

			key = channels[i];
			
			if(key != data.content){

				talks.notice(notice, key, data.sender.nick);
				talks.get(key).setMember(data.sender.nick, 'name', data.content);
			}
		}
	}

};

Commands.prototype.PRIVMSG = function (data) {
	//Se não for uma mensagem enviada pelo cliente
	if (data.sender.nick != chat.client.nick) {
		//Se existir a conversa (canal)
		if (talks.exists(data.target)) {
			talks.message(data.content, data.target, data.sender.nick);
		}
		//Mensagem privada
		else if (data.target == chat.client.nick) {
			//Se a conversa existir
			if (talks.exists(data.sender.nick)) {
				talks.message(data.content, data.sender.nick, data.sender.nick);
			}
			//Senão
			else {
				var talk = new Talk(data.sender.nick, 'user');
				talks.add(talk);
				talks.message(data.content, data.sender.nick, data.sender.nick);
			}
		}
	}
};

Commands.prototype.NOTICE = function (data) {
	//Se não for uma mensagem enviada pelo cliente
	if (data.sender.nick != chat.client.nick) {
		//Se existir a conversa (canal)
		if (talks.exists(data.target)) {
			talks.message('Notice:'+data.content, data.target, data.sender.nick);
		}
		//Mensagem privada
		else if (data.target == chat.client.nick) {
			//Se a conversa existir
			if (talks.exists(data.sender.nick)) {
				talks.message('Notice:'+data.content, data.sender.nick, data.sender.nick);
			}
			//Senão
			else {
				var talk = new Talk(data.sender.nick, 'user');
				talks.add(talk);
				talks.message('Notice:'+data.content, data.sender.nick, data.sender.nick);
			}
		}
	}
};

Commands.prototype.TOPIC = function (data) {
	
	var channel = data.target;
	var topic = data.content;

	if (channel === talks.selected.id)
		$('#chat-topic').html(topic);
	
	talks.set(channel, 'status', topic);
	//talks.notice('Tópico do canal: '+topic, channel);
};