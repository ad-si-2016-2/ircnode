/* Tratamento de mensagens numéricas do servidor IRC */

function Replies(chat){
	
	this.chat = chat;

	this.execute = function(data){
		//Confere se existe tratamento para esse comando, se existir executa
		if (typeof this["R"+data.id] === 'function') this["R"+data.id](data);
	};
}

//RPL_WELCOME
Replies.prototype.R001 = function(data){
	var nick = data.content.match(/([\w\-_]+)!(.*)@(.*)/)[1];
	chat.client.nick = nick;
	$('#user-nickname').val(nick);
};

//RPL_AWAY
Replies.prototype.R301 = function(data){

	var match = data.content.match(/^([\w\-_]+) :(.*)/);
	var nick = match[1];
	var away = match[2];

	if(talks.get(nick)){

		talks.set(nick, 'status', 'ausente · '+away);

		if(nick === talks.selected.id)
			talks.topic();
	}
};

//RPL_IDLE
Replies.prototype.R317 = function(data){

	var match = data.content.match(/^([\w\-_]+) ([\d]+)(.*)/);
	var nick = match[1];
	var idle = parseInt(match[2] / 60);

	if(talks.get(nick)){

		if(idle < 5)
			talks.set(nick, 'status', 'online');
		else if(idle < 60)
			talks.set(nick, 'status', 'visto '+ idle +' minutos atrás');
		else
			talks.set(nick, 'status', 'ausente');

		if(nick === talks.selected.id)
			talks.topic();
	}	
};

//RPL_TOPIC
Replies.prototype.R332 = function(data){

	var match = data.content.match(/(.*) :(.*)/);
	var channel = match[1];
	var topic = match[2];

	if (channel === talks.selected.id)
		$('#chat-topic').html(topic);
	
	talks.set(channel, 'status', topic);
	//talks.notice('Tópico do canal: '+topic, channel);
};

//RPL_NAMES
Replies.prototype.R353 = function(data){
	
	var match = data.content.match(/(.*):(.*)/);
	var users = match[2].split(' ');
	var channel = match[1].match(/(.*)(([#&!+])(.*)) /)[2];

	if(talks.exists(channel)){
	
		for (i=0; i<users.length; i++) {
			if (users[i][0] == '@')
				talks.addMember(channel, users[i].slice(1, users[i].length), '@');
			else 
				talks.addMember(channel, users[i]);
		}
		
		if(talks.selected.id == channel)
			talks.topic();
	}
};

//ERR_NOSUCHNICK
Replies.prototype.R401 = function(data) {

	var nick = data.content.match(/^([^\s]+) :(.*)/)[1];

	if(talks.get(nick)){

		talks.set(nick, 'status', 'offline');

		if(nick === talks.selected.id)
			talks.topic();
	}
};

//ERR_NONICKNAMEGIVEN
Replies.prototype.R431 = function(data){

	$('#user-nickname').val(chat.client.nick);
};

//ERR_ERRONEOUSNICKNAME
Replies.prototype.R432 = function(data){

	$('#user-nickname').val(chat.client.nick);
};

//ERR_NICKINUSE
Replies.prototype.R433 = function(data){

	$('#user-nickname').val(chat.client.nick);
};

