/* WEB IRC */

//Variáveis globais
var client = {
	pass: $.md5(new Date()), 
	user: "web_client", 
	host: "web.irc.inf.ufg.br", 
	realname: "guest", 
	nickname: "WIRED", 
	channel: "#ad-si-2016-2"
};

var server = {
	host: "localhost", 
	port: 8081, 
	service: "rest", 
	name: "Servidor IRC"
};

var talks    = [];       //Lista de conversas do usuário
var selected = 'server'; //Conversa atualmente selecionada
var channels = [];       //Lista com todos os canais do servidor
var interval = null;     //Intervalo de acesso ao servidor
var errors   = 0;        //Marca os erros recebidos (a partir de 3 seguidas considera o servidor fora do ar)

var lastLine = '';
var nextLine = '';

talks['server'] = {
	name: 'Servidor IRC', 
	type: 'server', 
	messages: [], 
	content: ''
};


//Inicialização
$(document).ready(function(){

	//Registro de conexão IRC
	connect();

	//Seta intervalo de acesso ao servidor
	interval = setInterval(function(){send('PING')}, 1000);


	//Seleção de conversa
	$(".conversation").click(function(){
		conversationClick(this);
	});

	//Envio de texto
	$("#message-text").keydown(function(e){

		//Se apertar Enter e o Shift não estiver apertado
		if ((e.keyCode == 13)&&(!e.shiftKey)) {

			var text = $(this).val();

			//Reseta o campo
			$(this).val('');
			e.preventDefault();

			//Se houver texto
			if(text){

				//Se for a aba do servidor, envia comando
				if(selected === 'server'){
					var params = text.match(/^(\/|)([\w]+)($| (.*))/);
					
					if(params[4])
						send(params[2], params[4].split(" "));
					else
						send(params[2]);
				}

				//Senão envia mensagem de texto para conversa selecionada
				else{
					send("PRIVMSG", [selected, text]);
				}

				//Adiciona a mensagem e printa na tela
				addMessage('me', selected, text);
				lastLine = text;
			}
		}
		else if(e.keyCode == 38){
			if($(this).val() != lastLine){
				nextLine = ($(this).val())? $(this).val() : " ";
				$(this).val(lastLine);
			}
		}
		else if(e.keyCode == 40){
			if(nextLine){
				$(this).val(nextLine);
				nextLine = '';
			}
		}
	});
});


function conversationClick(div){
	$('.conversation.selected').removeClass('selected');
	$(div).addClass('selected');
	$(div).removeClass('new-messages');
	talks[selected].content = $('#chat').html();
	selected = $(div).attr('name');
	$('#chat-name').html($(".conversation[name='"+selected+"'] .conversation-name").html());
	$('#chat').html(talks[selected].content);
	$('#chat-container').scrollTop($('#chat').height());
	$('#message-text').focus();
}

function addMessage(sender, talk, msg){

	var messages = talks[talk].messages;

	//Informações da hora
	var date   = new Date();
	var hour   = date.getHours();
	var minute = date.getMinutes();

	hour   = (hour<10)?   '0'+hour : hour;
	minute = (minute<10)? '0'+minute : minute;

	var time = hour+":"+minute;

	if(sender === 'me'){
		messages.push({
			sender: client.nickname, 
			type: sender, 
			time: time, 
			content: msg
		});
	}
	else if(sender === 'server'){
		messages.push({
			sender: server.name, 
			type: sender, 
			time: time, 
			content: msg
		});
	}
	else{
		messages.push({
			sender: sender, 
			type: 'normal', 
			time: time, 
			color: colorize(sender), 
			content: msg
		});
	}

	if(talk == selected){
		printMessage(messages[messages.length-1]);
	}
	else{
		$(".conversation[name='"+talk+"'").addClass("new-messages");
	}


	$('.conversation[name="'+talk+'"] .conversation-info').html(msg);
}

//Gera uma cor baseada no nickname
function colorize(nick){

	var code = 0;
	
	for(i=0; i<nick.length; i++){
		code += nick.charCodeAt(i);
	}

	code %= 5;
	
	return code+1;
}

//Registro de conexão
function connect(){

	//PASS: Obrigatório para o cliente Web ser identificado
	send("PASS", client.pass);

	//NICK: Opcional, caso não haja Nickname, recebe nome aleatório
	if(client.nickname)
		send("NICK", client.nickname);

	//USER: Completa o registro de conexão
	send("USER", [client.user, "0", "*", client.realname]);
	
	//JOIN: Caso haja canal preferencial, se conecta automaticamente
	if(client.channel)
		send("JOIN", client.channel);
}

//Envio de comandos para o servidor
function send(operation, args){

	//Sem parâmetros
	if (!args) {
		var params = "";
	}
	//Um parâmetro (string)
	else if (typeof(args)==='string') {
		var params = encodeURIComponent(args) + "/";
	}
	//Mais de um parâmetro (array)
	else {
		var params = "";
		args.forEach(function(arg){
			params += encodeURIComponent(arg) + "/";
		});
	}

	operation = operation.toUpperCase();
	
	var url = "http://"+server.host+":"+server.port+"/"+server.service+"/"+client.pass+"/"+operation+"/"+params;

	$.ajax({
		type: "GET", 
		dataType: "json", 
		url: url
	}).done(function(responses){ 

		console.log(responses);

		errors = 0;

		responses.forEach(function(response){
			//Seta o tipo da origem (usuário ou servidor)
			response.origin = (response.sender.match(/^[\w-_]+!(.*)@(.*)/))? 'user' : 'server';
			
			if(response.origin == 'user'){
				var match = response.sender.match(/^(.*)!(.*)@(.*)$/);
				response.nick = match[1];
				response.user = match[2];
				response.host = match[3];
				if(response.nick == client.nickname) response.origin = 'me';
			}
			
			response.content = response.content.replace(/^:/, '');

			//Reply
			if(response.id.match(/\d\d\d/)) reply(response);
			
			//Command
			else command(response);
		});

	}).fail(function(err){

		console.log(errors);
		
		errors++;
		
		if(errors >= 3){
			clearInterval(interval);
		}
	});
}

function reply(msg){

	console.log('reply');
	
	switch(msg.id){

		case '001':
			var match = msg.content.match(/^(.*) ([\w\-_]+)!(.*)@(.*)$/);
			client.nickname = match[2];
		break;


	}

	addMessage('server', 'server', "["+ ((msg.nick)? msg.nick : msg.sender) +"] "+ msg.id +" "+ msg.content);

}

function command(msg){

	console.log("command");

	switch(msg.id){

		case "NICK":
			if(msg.nick == client.nickname){
				client.nickname = msg.content;
			}
		break;

		case "JOIN":
			if(msg.nick == client.nickname){
				talks[msg.target] = {
					name: msg.target.slice(1, msg.target.length), 
					members: [], 
					type: 'channel', 
					prefix: msg.target[0], 
					opers: [], 
					color: colorize(msg.target), 
					messages: [], 
					content: ''
				}
				addConversation(talks[msg.target]);
				//addNotification();
			}
		break;

		case "PRIVMSG":
			if(msg.target === client.nickname){
				
				if(!talks[msg.nick]){ 
					talks[msg.nick] = {
						name: msg.nick, 
						type: 'private', 
						color: colorize(msg.target), 
						messages: [], 
						content: ''
					};
					addConversation(talks[msg.nick]);
				}

				addMessage(msg.nick, msg.nick, msg.content);
			}
			else if(msg.nick != client.nickname){
				addMessage(msg.nick, msg.target, msg.content);
			}
		break;

	}



	if((msg.id != "PONG")&&(msg.sender == 'server')){
		addMessage('server', 'server', "["+ ((msg.nick)? msg.nick : msg.sender) +"] "+ msg.id +" "+ msg.content);
	}
}


function addConversation(cv){

		if(cv.type == 'channel'){
			symbol = cv.prefix;
			name   = cv.prefix + cv.name;
		}
		else if(cv.name.match(/[\-_]/)){
			symbol = cv.name.split(/[\-_]/)[0][0].toUpperCase() + cv.name.split(/[\-_]/)[1][0].toUpperCase();
			name   = cv.name;
		}
		else{
			symbol = cv.name[0];
			name   = cv.name;
		}

		var newCv = 
		"<div class='conversation' name='"+name+"'><div class='avatar-container'>"+
		"<div class='avatar a"+cv.color+"'>"+symbol+"</div></div>"+
		"<div class='conversation-content'><div class='conversation-name'>"+cv.name+"</div>"+
		"<div class='conversation-info'></div></div></div>";

		$(newCv).insertAfter(".conversation[name='server']");

		$('.conversation').unbind('click');
		$('.conversation').bind('click', function(){ conversationClick(this) });

}


function printMessage(msg){


	//Incorpora hyperlinks
	var message = msg.content.replace(/</g, '&lt;').replace(/>/g, '&gt;');
	message = message.replace(
		/((https?:\/\/|)(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&\/\/=]*))/g, 
		"<a href='$1' target='_blank'>$1</a>"
	);

	//Incorpora vídeo do youtube
	var append = message.match(/(http(?:s?):\/\/(?:www\.)?youtu(?:be\.com\/watch\?v=|\.be\/)([\w\-\_]*)(&(amp;)?‌​[\w\?‌​=]*)?)/);
	if(append){ 
		message += '<iframe class="youtube" src="https://www.youtube.com/embed/'+ append[2] +'" allowfullscreen></iframe>';
	}
	//Incorpora imagem
	else{
		append = message.match(/([a-z\-_0-9\/\:\.]*\.(jpg|jpeg|png|gif))/i);

		if(append){

			message += '<img class="image" style="display:none" src="'+append[1]+'"/>';

			imageExists(append[1], function(exists) {
				if(!exists) $('#chat .message img:last').remove();
				else{ 
					$('#chat .message img:last').show();
					$("#chat-container").stop().animate({scrollTop: $('#chat').height()}, 400);
				}
			});
		}

		function imageExists(url, callback) {
		  var img = new Image();
		  img.onload = function() { callback(true); };
		  img.onerror = function() { callback(false); };
		  img.src = url;
		}
	}



	//Se já existir um balão atual, insere dentro dele
	if(($('#chat .message:last .message-time').html() == msg.time)&&($('#chat .message:last .message-name').html() == msg.sender)){
		$("<div class='message-content'>"+msg.content+"</div>").insertAfter('#chat .message:last .message-content:last');
	}
	//Senão cria um novo balão
	else{
		//Monta a mensagem
		$('#chat').append("<div class='message-container'><div class='message "+msg.type+"'>"+
						  "<div class='message-name n"+msg.color+"'>"+msg.sender+"</div>"+
						  "<div class='message-content'>"+message+"</div>"+
						  "<div class='message-time'>"+msg.time+"</div></div></div>");
	}
	
	$("#chat-container").stop().animate({scrollTop: $('#chat').height()}, 400);

}