/* WEB IRC */

// Inicialização da página
$(document).ready(function(){
	
	//Seta classes de processamento de dados
	chat.commands = new Commands(chat);
	chat.replies  = new Replies(chat);

	//Inicia comunicação com servidor
	chat.start(250);
	
	//Seta eventos do chat
	events = new Events();
	events.bind();

	//Cria janela de conversação com o servidor
	var server = new Talk('Servidor IRC', 'server');
	server.set('status', chat.server.stat);
	talks.add(server, true);

	//Importa emotes
	$.getJSON('json/emotes.json').done(function(data){
		emotes = data;
	});
});

// Propriedades do chat
var chat = {

	client: {
		pass: $.md5(new Date()), 
		user: "web_client", 
		host: "web.irc.inf.ufg.br", 
		nick: "WIRED", 
		name: "guest", 
		chan: "#ad-si-2016-2"
	}, 
	server: {
		host: "localhost", 
		port: 8081, 
		rest: "rest", 
		name: "Servidor IRC", 
		stat: "Envie comandos do protocolo IRC"
	}, 
	interval: null, 
	errors: 0, 
	lastLine: '', 
	nextLine: '', 
	sendList: []
};

// Método de inicialização do chat
chat.start = function(time){

	//Registra conexão com o servidor IRC
	chat.connect();

	//Seta o intervalo de acesso ao servidor
	chat.interval = setInterval(function(){
		chat.getMessages();
	}, time);
};

// Registro de conexão IRC	
chat.connect = function(){

	//PASS: Obrigatório para o cliente Web ser identificado
	chat.sendCommand("PASS", chat.client.pass);

	//NICK: Opcional, caso não haja Nickname, recebe nome aleatório
	if(chat.client.nick)
		chat.sendCommand("NICK", chat.client.nick);

	//USER: Completa o registro de conexão
	chat.sendCommand("USER", [chat.client.user, "0", "*", chat.client.name]);
	
	//JOIN: Caso haja canal preferencial, se conecta automaticamente
	if(chat.client.chan)
		chat.sendCommand("JOIN", chat.client.chan);
};

// Envia comando para o servidor
chat.sendCommand = function(command, args){

	//Trata comando
	command = ((command)&&(typeof command != 'undefined'))? command.toUpperCase() : false;
	
	//Trata argumentos
	var params = (args)? args : [];
	
	if(typeof args == 'string'){

		params = [args];
	}

	//Adiciona na fila
	if ((command)&&(chat.sendList.length)) {

		this.sendList.push({command: command, params: params});
	}
	//Envia comando
	else if ( ((!chat.sendList.length)&&(command)) || ((!command)&&(chat.sendList.length)&&(chat.sendList[0])) ) {

		//Extrai as informações da fila
		if ((!command)&&(chat.sendList.length)) {
			command = chat.sendList[0].command;
			params  = chat.sendList[0].params;
			chat.sendList[0] = false;
		}
		//Seta "comando em execução"
		else {
			chat.sendList.push(false);
		}

		var url = "http://"+chat.server.host+":"+chat.server.port+"/"+chat.server.rest+"/"+command;
		
		$.post(url, {
			pass: chat.client.pass,
			args: params
		})
		.done(function(){

			//Remove da fila
			chat.sendList.splice(0,1);
			
			//Se tiver algum na fila, executa
			if(chat.sendList.length)
				chat.sendCommand();
		})
		.fail(function(e){
			//Trata erro
			chat.error(e);
		});
	}
};

// Recupera mensagens do servidor
chat.getMessages = function(){

	var url = "http://"+chat.server.host+":"+chat.server.port+"/"+chat.server.rest+"/"+chat.client.pass
			
	$.get(url)
	.done(function(data){
		
		//Envia respostas para tratamento
		if(data){
			chat.data(JSON.parse(data));
		}
	})
	.fail(function(e){
		//Trata erro
		chat.error(e);
	});
};

// Trata o recebimento de dados do servidor
chat.data = function(responses){

	errors = 0;

	responses.forEach(function(response){
		if(response.sender && response.id){

			console.log(response);

			//Printa response no servidor
			if (response.sender.type === 'server')
				talks.message('['+response.sender.host+'] '+response.id+" "+response.content, 'server', chat.server.name);
			
			//Reply
			if(response.type === 'reply')   chat.replies.execute(response);
			
			//Command
			if(response.type === 'command') chat.commands.execute(response);
		}
	});
};

// Trata erros de comunicação com o servidor
chat.error = function(e){

	chat.errors++;

	if(chat.errors >= 3){
		console.log("Conexão com o servidor interrompida");
		clearInterval(chat.interval);
	}
};

// GET genérico de atributos do chat
chat.get = function(attr){
	if(attr in chat) return chat[attr];
    else return false;
};

// SET genérico de atributos do chat
chat.set = function(attr, value){

	if(attr in chat) chat[attr] = value;
};
