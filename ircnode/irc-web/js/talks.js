/* Objeto responsável pelas janelas de conversação */

var talks = new Talks();

function Talks() {
	this.talks = {};
	this.selected = null;
}

// talks.add(talk{});
Talks.prototype.add = function(item, select){
	
	//Adiciona ao array de conversas
	this.talks[item.id] = item;
	
	var symbol = this.symbol(item);

	var conversation = 
	"<div class='conversation' name='"+item.id+"'><div class='avatar-container'>"+
	"<div class='avatar a"+item.color+"'>"+symbol+"</div></div>"+
	"<div class='conversation-content'><div class='conversation-name'>"+item.name+"</div>"+
	"<div class='conversation-info'></div></div></div>";

	if(item.get('type') === 'server')
		$("#talks").html(conversation);
	else
		$(conversation).insertAfter(".conversation[name='server']");

	//Reseta os eventos
	events.bind();

	//Se for autoseleção
	if(select) this.select(item.id);
};

Talks.prototype.symbol = function(item){
	//Adiciona na tela
	if(item.type === 'channel'){
		symbol = item.prefix;
	}
	else if(item.name.match(/[\-_]/)){
		symbol = item.name.split(/[\-_]/)[0][0].toUpperCase() + item.name.split(/[\-_]/)[1][0].toUpperCase();
	}
	else{
		symbol = item.name[0];
	}
	return symbol.toUpperCase();
}

// talks.remove(talk.id);
Talks.prototype.remove = function(id){
	if(this.talks[id]) delete this.talks[id];
	$('.conversation[name="'+id+'"]').remove();
};

//Seleciona uma conversa
Talks.prototype.select = function(id){

	if((!this.selected)||(id != this.selected.id)){

		this.selected = this.talks[id];
		
		var select = $('.conversation[name="'+id+'"]');
		
		$('.conversation.selected').removeClass('selected');
		
		select.addClass('selected');
		select.removeClass('new-messages');
		
		$('#chat-name').html(this.selected.get('name'));

		this.selected.print();

		if(this.selected.type == 'user') this.status(id);
	}

	this.topic();
	
	$('#message-text').focus();
};

// talks.isSelected(talk.id);
Talks.prototype.isSelected = function(id){

	return (this.selected.id === id);
};

// talks.get(talk.id);
// talks.get(talk.id, attr);
Talks.prototype.get = function(id, attr){
	
	if((!attr)&&(this.talks[id])) 
		return this.talks[id];

	else if (this.talks[id]) 
		return this.talks[id].get(attr);

	else 
		return false;
};

// talks.set(talk.id, talk{});
// talks.set(talk.id, talk.attr, value);
Talks.prototype.set = function(id, item, value){
	if(talks.talks[id])
		talks.talks[id].set(item, value);
};

// talks.exists(talk.id);
Talks.prototype.exists = function(id){

	return (this.talks[id])? true : false; 
};

// talks.message(msg, [target], sender);
Talks.prototype.message = function(msg, target, sender){

	var selected = this.selected.id;

	if(!target){
		Object.keys(this.talks).forEach(function(key) {
		    //Se for conversa atual, manda printar na tela
			var print = (key == selected);
			//Envia mensagem
			talks.talks[key].add(msg, sender, 'message', print);
		});
	}
	else {
		//Se for conversa atual, manda printar na tela
		var print = (target == this.selected.id);

		//Adiciona mensagem à conversa
		this.talks[target].add(msg, sender, 'message', print);
	}
};

// talks.notice(msg, [target]);
// talks.notice(msg, [target], sender);
Talks.prototype.notice = function(msg, target, sender){
	
	var selected = this.selected.id;
	sender = (sender)? sender: '';
	
	if(!target){
		Object.keys(this.talks).forEach(function(key) {
		    //Se for conversa atual, manda printar na tela
			var print = (key == selected);
			//Envia mensagem
			talks.talks[key].add(msg, sender, 'notice', print);
		});
	}
	else{
		//Se for conversa atual, manda printar na tela
		var print = (target == this.selected.id);

		//Adiciona mensagem à conversa
		this.talks[target].add(msg, sender, 'notice', print);
	}
};

Talks.prototype.member = function(name) {

	var channels = [];

	Object.keys(this.talks).forEach(function(key) {
	    
		if(talks.talks[key].isMember(name)){

			channels.push(key);
		}
	});

	return channels;
};

Talks.prototype.addMember = function(target, name) {
	talks.talks[target].addMember(name);
	this.topic();
};

Talks.prototype.removeMember = function(target, name) {

	talks.talks[target].removeMember(name);
	this.topic();
};

Talks.prototype.topic = function(){
	
	if(this.selected.status) {
		$('#chat-topic').html( this.selected.status );
	}
	else{
		$('#chat-topic').html('');
	}

	if(this.selected.get('type') === 'channel'){
		var members = this.selected.get('members').length;
		var text =  members + ((members>1)? " membros" : " membro") + (($('#chat-topic').html())? ' · ' : ' ');
		$('#chat-members').html(text);
	}
	else{
		$('#chat-members').html('');
	}

};

Talks.prototype.status = function(id){
	chat.sendCommand('WHOIS', [id]);
};